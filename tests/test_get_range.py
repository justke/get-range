from unittest import TestCase

from src.get_range import get_range, get_range_collaboration


class TestGetRange(TestCase):

    def test_happy_path(self):
        result = get_range('2019-11', '2020-02')
        self.assertEqual(
            [
                ['2019-11-01', '2019-11-30'],
                ['2019-12-01', '2019-12-31'],
                ['2020-01-01', '2020-01-31'],
                ['2020-02-01', '2020-02-29'],
            ], result
        )


class TestGetRangeCollaboration(TestCase):

    def test_happy_path(self):
        result = get_range_collaboration('2019-11', '2020-02')
        self.assertEqual(
            [
                ['2019-11-01', '2019-11-30'],
                ['2019-12-01', '2019-12-31'],
                ['2020-01-01', '2020-01-31'],
                ['2020-02-01', '2020-02-29'],
            ], result
        )
