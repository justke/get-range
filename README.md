# Get Range Code Challenge

Given a start month and an end month provided as strings, generate a list of lists of the first and last day of every month.

Example function call:
```python
get_range('2019-11', '2019-02')
```

Example response:
```
[['2019-11-01', '2019-11-30'], ['2019-12-01', '2019-12-31'], ['2020-01-01', '2020-01-31'], ['2020-02-01', '2020-02-29']]
```
