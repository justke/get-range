from typing import Generator, List, Tuple
from calendar import monthrange
from datetime import date

from dateutil.relativedelta import relativedelta


def get_range(start: str, end: str) -> List[List[str]]:
    """
    A function that when provided a start date (month-year) as a str along with an end date of the
    same format, will return a list of lists, where each internal list is two items, a date string
    for the first day of the month and date string for the last day of the month.

    :param start: (str) - The start date to start generating the list (inclusive), yyyy-mm.
    :param end: (str) - The end date to start generating the list (inclusive), yyyy-mm.
    :return: (list) - A list of lists of first day and last day of the month.
    """
    start_year, start_month = map(int, start.split('-'))
    end_year, end_month = map(int, end.split('-'))
    months = ((end_year - start_year) * 12) + end_month - start_month + 1

    def make_range(year: int, month: int) -> List[str]:
        return [f'{year}-{month:02}-01', f'{year}-{month:02}-{monthrange(year, month)[1]}']

    def calculate_year(offset: int) -> int:
        return start_year + ((start_month - 1 + offset) // 12)

    def calculate_month(offset: int) -> int:
        return (start_month - 1 + offset) % 12 + 1

    return [make_range(calculate_year(i), calculate_month(i)) for i in range(months)]


def get_range_collaboration(start: str, end: str) -> List[List[str]]:
    """
    This solution was determined through collaboration with a coworker.
    """

    def make_range(year: int, month: int) -> List[str]:
        return [f'{year}-{month:02}-01', f'{year}-{month:02}-{monthrange(year, month)[1]}']

    def make_date_tuple(_date: date) -> Tuple[int, int]:
        return (_date.year, _date.month)

    def date_stream() -> Generator[date, None, None]:
        start_year, start_month = map(int, start.split('-'))
        end_year, end_month = map(int, end.split('-'))
        current = date(start_year, start_month, 1)
        while current < date(end_year, end_month, 1) + relativedelta(months=1):
            yield current
            current += relativedelta(months=1)

    dates = date_stream()
    return [make_range(*date) for date in map(make_date_tuple, dates)]
